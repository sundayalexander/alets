<?php
/**
*@Author Amowe Sunday Alexander
* This mail class in used to perform
* SMTP mail only.
* POP,IMAP Protocol are not implemented here.
*/
class Mail{
	//properties
	private $to, $from, $message, $address, $subject,$header = "", $replyTo, $isAttached = false;
	private static $eol = PHP_EOL;
	
	/**
	*This method Set the sender mail.
	*@param address: this is the senders
	*email address.
	*@throw Invalid Email: this is 
	*Thrown if the mail address provided
	* is not a valid address format.
	*/
	public function setSender($address){
		if(!filter_var($address, FILTER_VALIDATE_EMAIL)){
			throw new InvalidArgumentException("The provided Sender address does not comfirm to the basic mail address format");
		}else{
			$this->from = $address;
			$this->header = "From: ".$this->from.Mail::$eol;
			
		}
		
	}
	/**
	*This method Set the reply-to mail address.
	*@param address: this is the senders
	*email address.
	*@throw Invalid Email: this is 
	*Thrown if the mail address provided
	* is not a valid address format.
	*/
	public function setReplyAddress($address){
			$this->replyTo = $address;
			$this->header .= "Reply-To: ".$this->replyTo.Mail::$eol;		
	}
	
	/**
	*This method Set the Reciever email address.
	*@param address: this is the senders
	*email address.
	*@throw InvalidArgumentException: this is 
	*Thrown if the mail address provided
	* is not a valid address format.
	*/
	public function setRecipient($address){
		if(!filter_var($address, FILTER_VALIDATE_EMAIL)){
			throw new InvalidArgumentException("The provided Recipient address does not comfirm to the basic mail address format");
		}else{
			$this->to = $address;
		}
	}
	
	/**
	*This method Set the email subject.
	*@param subject: this is the subject
	* of the email message.
	*/
	public function setSubject($subject){
		$this->subject = $subject;
	}
	
	/**
	*This method Set the mail message.
	*@param message: this is the senders
	*email address.
	*/
	public function setMessage($message){
		$this->message = $message;
	}
	
	/**
	*This method Set the mail attachment.
	*@return: this is set to true if the 
	*mail was sent, false if otherwise.
	*/
	public function send(){
		if($this->isAttached){
			if(mail($this->to,$this->subject,$this->message,$this->header)){
				return true;
			}else {
				return false;
			}
		}else{
			if(mail($this->to,$this->subject,$this->message,$this->header)){
				return true;
			}else {
				return false;
			}
		}
		
	}
	
	/**
	*This method Set the mail attachment.
	*@param attachment: this is the senders
	*email address.
	*/
	public function setAttachment($fileURL) {
		$fileInfo = pathinfo($fileURL);
		$fileName = $fileInfo["basename"];
		 $file_size = filesize($fileURL);
		 $handle = fopen($fileURL, "r");
		 $content = fread($handle, $file_size);
		 fclose($handle);
		 $content = chunk_split(base64_encode($content));
		 $uid = md5(uniqid(time()));
		 //set the headers
		 $this->header .= "MIME-Version: 1.0\r\n";
		 $this->header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"";
		 
		 $body = "--".$uid.Mail::$eol;
		 $body .= "Content-type:text/plain; charset=iso-8859-1".Mail::$eol;
		 $body .= "Content-Transfer-Encoding: 7bit".Mail::$eol.Mail::$eol;
		 $body .= $this->message.Mail::$eol;
		 $body .= "--".$uid.Mail::$eol;
		 $body .= "Content-Type: application/octet-stream; name=\"".$fileName."\"".Mail::$eol; 
		 $body .= "Content-Transfer-Encoding: base64".Mail::$eol;
		 $body .= "Content-Disposition: attachment; filename=\"".$fileName."\"".Mail::$eol;
		 $body .= $content.Mail::$eol;
		 $body .= "--".$uid."--";
		 $this->message = $body;
		 $body = "";
		 $this->isAttached = true;
	
	}
}
?>