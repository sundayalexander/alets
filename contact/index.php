<?php
	require_once("../server/Mail.class.php");
	$status = "";
	if(!empty($_POST["name"]) && !empty($_POST["email"]) && !empty($_POST["message"]) && 
	filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)){
		$mail = new Mail();
		$name = filter_var($_POST["name"], FILTER_SANITIZE_STRING);
		$email = $_POST["email"];
		$message = filter_var($_POST["message"], FILTER_SANITIZE_STRING);
		$mail->setSubject("Contact message from ".$name);
		$mail->setSender($email);
		$mail->setRecipient("info@alets.com.ng");
		$mail->setMessage($message);
		if($mail->send()){
			$status = "Thank you for contacting us, we'll get back to you soon.";
		}else{
			$status = "Sorry! we're unable to process your message at the moment, please try agian soon.";
		}
	}else{
		header("location: ../contact.html",true);	
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Contact Us</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
</head>
<body>
<p><?=$status;?>
</body>
</html>